import { Component, OnInit } from '@angular/core';
import { MenuService } from '../helpers/menu.service';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  constructor(public menu: MenuService) {}

  items!: MenuItem[];
  
  ngOnInit() {
      this.items = [
        {
            label: 'Портфель проектов',
            icon: 'pi pi-pw pi-file',
            items: [
                {label: 'Реестр проектов', icon: 'pi pi-fw pi-user-plus', route: '/project-register'},
                {label: 'Отчёт по портфелю', icon: 'pi pi-fw pi-filter', route: '/project-portfolio'},
                // {label: 'Open', icon: 'pi pi-fw pi-external-link'},
                // {separator: true},
                // {label: 'Quit', icon: 'pi pi-fw pi-times'}
            ]
        },
        {
            label: 'Проект',
            icon: 'pi pi-fw pi-pencil',
            items: [
                {label: 'СДР', icon: 'pi pi-fw pi-trash', route: '/currency'},
                {label: 'Параметры проекта', icon: 'pi pi-fw pi-refresh', route: '/currency'},
                {label: 'График проекта', icon: 'pi pi-fw pi-refresh', route: '/currency'},
                {label: 'Команда проекта', icon: 'pi pi-fw pi-refresh', route: '/currency'}
            ]
        },
        {
            label: 'ГПКС / ГПФКС',
            icon: 'pi pi-fw pi-question',
            items: [
                {
                    label: 'Contents',
                    icon: 'pi pi-pi pi-bars',
                    route: '/currency'
                },
                {
                    label: 'Search', 
                    icon: 'pi pi-pi pi-search', 
                    items: [
                        {
                            label: 'Text', 
                            items: [
                                {
                                    label: 'Workspace'
                                }
                            ]
                        },
                        {
                            label: 'User',
                            icon: 'pi pi-fw pi-file',
                        }
                ]}
            ]
        },
        {
            label: 'Справочники',
            icon: 'pi pi-fw pi-cog',
            items: [
                {
                    label: 'Бюджет',
                    icon: 'pi pi-fw pi-pencil',
                    items: [
                        {label: 'Объекты аналоги', icon: 'pi pi-fw pi-save', route: '/analog-objects'},
                        {label: 'Коэффициент сложности', icon: 'pi pi-fw pi-save', route: '/difficulty-factor'},
                        {label: 'Макропоказатели', icon: 'pi pi-fw pi-save', route: '/macro-indicators'},
                    ]
                },
                {
                    label: 'Проект',
                    icon: 'pi pi-fw pi-tags',
                    items: [
                        {label: 'Нематериальные активы', icon: 'pi pi-fw pi-minus', route: '/'},
                        {label: 'Общепроектные затраты', icon: 'pi pi-fw pi-minus', route: '/'},
                        {label: 'Категории', icon: 'pi pi-fw pi-minus', route: '/category'},
                        {label: 'Валюты', icon: 'pi pi-fw pi-minus', route: '/currency'},
                        {label: 'Регионы', icon: 'pi pi-fw pi-minus', route: '/regions'},
                        {label: 'Роли', icon: 'pi pi-fw pi-minus', route: '/role'}
                    ]
                },
                {
                  label: 'Сметы',
                  icon: 'pi pi-fw pi-tags',
                  items: [
                      {label: 'Укрупнение сметных позиций', icon: 'pi pi-fw pi-minus', route: '/'},
                      {label: 'Общепроектные затраты', icon: 'pi pi-fw pi-minus', route: '/'}
                  ]
              }
            ]
        }
    ];
  }
}
