export interface ProjectRegister {
    id: number;
    code_wbs: string;
    name: string;
    user_created: number;
}  