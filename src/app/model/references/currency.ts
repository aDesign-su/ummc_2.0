export interface Currency {
    id: number
    title: string
    editing?: any
}
