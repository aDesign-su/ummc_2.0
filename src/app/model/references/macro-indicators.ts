export interface KeyResult {
    'id': number;
    'param_1': string;
    'parameter': string;
    'param_3': string;
    [year: string]: string | number;
}
export interface CurrencyExchange {
    id: number;
    year: number;
    parameter: {
      id: number;
      type_parameter: string;
      name: string;
      base: string;
    };
    value: string;
}
export interface Wacc {
    parameter: string
    source: string
    value: string
}
export interface Parameters {
    type_parameter: string
    name: string
    base: string
}