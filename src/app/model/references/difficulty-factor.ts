export interface DifficultyFactor {
    id: any
    number: string,
    rate: null,
    title: string,
    comments: string,
    estimated_budget: number
    editing?: any
}
