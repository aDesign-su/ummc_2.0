export interface analogBudgets {
    currency: string;
    currency_id: number;
    date_end: string;
    date_start: string;
    etc_cost: string;
    id: number;
    oto_cost: string;
    pir_comp: string;
    pir_cost: string;
    project: string;
    region: string;
    region_id: number;
    smr_comp: string;
    smr_cost: string;
    tech_description: string;
    title: string;
    total_cost: string;
    type: string;
    type_id: number;
    children: 
        {
            analog_budget_equipment: Equipments[]
            analog_budget_package: WorkPackages[]
        },
}
export interface WorkPackages {
    id: number
    title: string,
    type: string,
    contractor: string,
    cost_total: number,
    cost_pp: number,
    mh: number,
    comments: string,
    project: number,
    region: number
}
export interface Equipments {
    id: number
    title: string,
    type: string,
    tech_description: string,
    cost: number,
    description: string,
    project: number,
    manufacturer: number,
    estimated_budget: number
}