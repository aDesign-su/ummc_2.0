import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ProjectRegister } from '../model/project-portfolio/project-registry';

@Injectable({
  providedIn: 'root'
})
export class ProjectPortfolioService {

  constructor(private http: HttpClient) { }
  
  /** Реестр проектов */
  getProjectRegister() {
    return this.http.get<ProjectRegister[]>(`${environment.apiUrl}/get_projects/`);
  }
}
