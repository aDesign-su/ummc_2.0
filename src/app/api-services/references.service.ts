import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, map } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Equipments, WorkPackages, analogBudgets } from '../model/references/analog-objects';
import { DifficultyFactor } from '../model/references/difficulty-factor';
import { CurrencyExchange, Wacc, Parameters } from '../model/references/macro-indicators';
import { Currency } from '../model/references/currency';
import { Regions } from '../model/references/regions';
import { Role } from '../model/references/role';

@Injectable({
  providedIn: 'root'
})
export class ReferencesService {

constructor(private http: HttpClient) { }

/** Объекты аналоги */
  getAnalogList(): Observable<any> {
    return this.http.get(`${environment.apiUrl}/references/analog_budget_type/`)
  }
  getCurrencyList(): Observable<any> {
    return this.http.get(`${environment.apiUrl}/references/currency/`)
  }
  getRegionList(): Observable<any> {
    return this.http.get(`${environment.apiUrl}/references/region/`)
  }
  getUnitOfMeasureList(): Observable<any> {
    return this.http.get(`${environment.apiUrl}/references/unit_of_measure/`)
  }
  getAnalogBudget(): Observable<analogBudgets[]> {
    return this.http.get<analogBudgets[]>(`${environment.apiUrl}/analog_budget/analog_budget/`)
  }
  delAnalogBudget(analogBudgets: number): Observable<analogBudgets[]> {
    const url = `${environment.apiUrl}/analog_budget/analog_budget_update/${analogBudgets}`;
    return this.http.delete<analogBudgets[]>(url);
  }
  addAnalogBudget(analogBudget: analogBudgets): Observable<analogBudgets[]> {
    const url = `${environment.apiUrl}/analog_budget/analog_budget_create/`;
    return this.http.post(url, analogBudget).pipe(
      map((response: any) => response.body)
    );
  }
  editAnalogBudget(AnalogBudgetId: number, updatedData: any): Observable<analogBudgets[]> {
    const url = `${environment.apiUrl}/analog_budget/analog_budget_update/${AnalogBudgetId}/`;
    return this.http.put<analogBudgets[]>(url, updatedData);
  }
  getAnalogBudgetId(id: number): Observable<analogBudgets> {
    return this.http.get<analogBudgets>(`${environment.apiUrl}/analog_budget/analog_budget/${id}/`)
  }
  addEquipment(Equipment: Equipments): Observable<Equipments[]> {
    const url = `${environment.apiUrl}/analog_budget/analog_budget_equipment_create/`;
    return this.http.post(url, Equipment).pipe(
      map((response: any) => response.body)
    );
  }
  addWorkPackage(WorkPackage: WorkPackages): Observable<WorkPackages[]> {
    const url = `${environment.apiUrl}/analog_budget/analog_budget_package_create/`;
    return this.http.post(url, WorkPackage).pipe(
      map((response: any) => response.body)
    );
  }
  delEquipment1(analogBudgets: number): Observable<analogBudgets[]> {
    const url = `${environment.apiUrl}/analog_budget/analog_budget_equipment_update/${analogBudgets}/`;
    return this.http.delete<analogBudgets[]>(url);
  }
  delWorkPackage1(analogBudgets: number): Observable<analogBudgets[]> {
    const url = `${environment.apiUrl}/analog_budget/analog_budget_package_update/${analogBudgets}/`;
    return this.http.delete<analogBudgets[]>(url);
  }

/** Коэффициент сложности работ */
  getDifficultyFactor(): Observable<DifficultyFactor[]> {
    return this.http.get<DifficultyFactor[]>(`${environment.apiUrl}/references/complexity_factor/`)
  }
  addDifficultyFactor(data: DifficultyFactor): Observable<DifficultyFactor[]> {
    const url = `${environment.apiUrl}/references/complexity_factor_create/`;
    return this.http.post(url, data).pipe(
      map((response: any) => response.body)
    );
  }
  editDifficultyFactor(id: number, data: any): Observable<DifficultyFactor[]> {
    const url = `${environment.apiUrl}/references/complexity_factor_update/${id}/`;
    return this.http.put<DifficultyFactor[]>(url, data);
  }
  delDifficultyFactor(id: number): Observable<DifficultyFactor[]> {
    const url = `${environment.apiUrl}/references/complexity_factor_update/${id}/`;
    return this.http.delete<DifficultyFactor[]>(url);
  }

/** Макропараметры */
  getParameter(): Observable<CurrencyExchange[]> {
    return this.http.get<CurrencyExchange[]>(`${environment.apiUrl}/macroparameters/parameter_value/`)
  }
  getSelect(): Observable<Parameters[]> {
    return this.http.get<Parameters[]>(`${environment.apiUrl}/macroparameters/parameters/`)
  }
  getWacc(): Observable<Wacc[]> {
    return this.http.get<Wacc[]>(`${environment.apiUrl}/macroparameters/discount_rates/`)
  }

/** Валюта */
  getCurrencyReference() {
    return this.http.get<Currency[]>(`${environment.apiUrl}/references/currency/`);
  }

/** Регионы */
  getRegionReference() {
    return this.http.get<Regions[]>(`${environment.apiUrl}/references/region/`);
  }

/** Справочник ролей */
  getRoleReference() {
    return this.http.get<Role[]>(`${environment.apiUrl}/role_reference/roles/`);
  }
}


