import { TestBed } from '@angular/core/testing';

import { ProjectPortfolioService } from './project-portfolio.service';

describe('ProjectPortfolioService', () => {
  let service: ProjectPortfolioService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProjectPortfolioService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
