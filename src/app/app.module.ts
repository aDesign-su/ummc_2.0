import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ButtonModule } from 'primeng/button';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './general-content/home/home.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { APP_INITIALIZER } from '@angular/core';
import { PrimeNGConfig } from 'primeng/api';
import { provideAnimationsAsync } from '@angular/platform-browser/animations/async';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { PrimaveraComponent } from './general-content/primavera/primavera.component';
import { TreeTableModule } from 'primeng/treetable';
import { MenuComponent } from './menu/menu.component';
import { DropdownModule } from 'primeng/dropdown';
import { FormsModule } from '@angular/forms'
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MessagesModule } from 'primeng/messages';
import { MessageService } from './helpers/message.service';
import { AnalogObjectsComponent } from './general-content/references/analog-objects/analog-objects.component';
import { DifficultyFactorComponent } from './general-content/references/difficulty-factor/difficulty-factor.component';
import { MacroIndicatorsComponent } from './general-content/references/macro-indicators/macro-indicators.component';
import { MatTooltipModule } from '@angular/material/tooltip';
import { DialogModule } from 'primeng/dialog';
import { TableModule } from 'primeng/table';
import { ReferencesService } from './api-services/references.service';
import { InputTextModule } from 'primeng/inputtext';
import { CalendarModule } from 'primeng/calendar';
import { ReactiveFormsModule } from '@angular/forms';
import { DetailedComponent } from './general-content/references/analog-objects/detailed/detailed.component';
import { TabViewModule } from 'primeng/tabview';
import { RoleComponent } from './general-content/references/role/role.component';
import { RegionsComponent } from './general-content/references/regions/regions.component';
import { CurrencyComponent } from './general-content/references/currency/currency.component';
import { CategoryComponent } from './general-content/references/category/category.component';
import { PanelMenuModule } from 'primeng/panelmenu';
import { ProjectRegistryComponent } from './general-content/project-portfolio/project-registry/project-registry.component';

const initializeAppFactory = (primeConfig: PrimeNGConfig) => () => {
  primeConfig.ripple = true;
};

@NgModule({
  declarations: [				
    AppComponent,
      /** Прочее */
      HeaderComponent,
      HomeComponent,
      PrimaveraComponent,
      MenuComponent,
      /** Портфель проектов */
      ProjectRegistryComponent,
      /** Справочники */
      AnalogObjectsComponent,
        DetailedComponent,
      DifficultyFactorComponent,
      MacroIndicatorsComponent,
      RoleComponent,
      RegionsComponent,
      CurrencyComponent,
      CategoryComponent,
   ],
  imports: [
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    BrowserModule,
    MatIconModule,
    MatButtonModule,
    MatMenuModule,
    MatSnackBarModule,
    MatTooltipModule,
    AppRoutingModule,
    ButtonModule,
    TableModule,
    TreeTableModule,
    DropdownModule,
    MessagesModule,
    DialogModule,
    TabViewModule,
    InputTextModule,
    CalendarModule,
    FormsModule,
    PanelMenuModule,
  ],
  providers: [
    {
       provide: APP_INITIALIZER,
       useFactory: initializeAppFactory,
       deps: [PrimeNGConfig],
       multi: true,
       
    },
    MessageService,
    ReferencesService,
    provideAnimationsAsync(),
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
