import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ReferencesService } from 'src/app/api-services/references.service';
import { DialogService } from 'src/app/helpers/dialog.service';
import { MenuService } from 'src/app/helpers/menu.service';
import { MessageService } from 'src/app/helpers/message.service';
import { CurrencyExchange, KeyResult, Wacc, Parameters } from 'src/app/model/references/macro-indicators';

@Component({
  selector: 'app-macro-indicators',
  templateUrl: './macro-indicators.component.html',
  styleUrls: ['./macro-indicators.component.scss']
})
export class MacroIndicatorsComponent implements OnInit {
  constructor(private reference: ReferencesService,private formBuilder: FormBuilder,public menu:MenuService,private toast:MessageService, public dialog: DialogService) { }

  ngOnInit() {
    this.Parameter();
    this.ParamData();
    this.Wacc();
  }

  theader = ['№ П.П.','Категория','Параметр','Тип','2024','2025','2026','2027','2028','2029','2030','2031','2032', 'Действие']
  theaderP = ['№ П.П.','Категория','Параметр','Тип', 'Действие']
  theaderW = ['№ П.П.','Показатель','Источник','Процент', 'Действие']
  dataKey!: KeyResult[]
  dataCurrencies!: KeyResult[]
  dataMetals!: KeyResult[]
  dataInflation1!: KeyResult[]
  dataInflation2!: KeyResult[]
  getWacc!: Wacc[]
  paramList!: Parameters[]
  flatData!: any[]
  arr: any[] = []

  Parameter() {
    this.reference.getParameter().subscribe(
      (response: CurrencyExchange[]) => {
        // this.key = response
        this.flatData = response.map((item: any) => ({
          id: item.id,
          year: item.year,
          parameter_id: item.parameter.id,
          parameter_type: item.parameter.type_parameter,
          parameter_name: item.parameter.name,
          parameter_base: item.parameter.base,
          value: item.value
        }));

      // Проходим по каждому элементу flatData и создаем новый объект для каждого уникального года с уникальным yid
      this.flatData.forEach(item => {
        const year = item.year.toString();
        const value = parseFloat(item.value).toFixed(2);
        const parameter1 = item.parameter_type;
        const parameter2 = item.parameter_name;
        const parameter3 = item.parameter_base;
        const id = item.parameter_id;
        const yid = item.id; // Используем идентификатор элемента как yid

        // Проверяем, существует ли уже объект с таким id в массиве arr
        let obj = this.arr.find((element:any) => element['id'] === id);
  
        // Если объект с таким id еще не существует, создаем его
        if (!obj) {
          obj = {
            // 'yid': yid, // Уникальный идентификатор элемента
            'id': id,
            'param_1': parameter1,
            'param_2': parameter2,
            'param_3': parameter3
          };
          this.arr.push(obj);
        }
        // Добавляем свойство с текущим yid в объект для текущего года
        obj[year] = value;
        obj[year + '_id'] = yid;
      });

        // Фильтруем массив arr по полю param_1
        let currency = this.arr.filter(item => item.param_1 === 'Курсы валют');
        this.dataCurrencies = currency;

        let metals = this.arr.filter(item => item.param_1 === 'Металлы');
        this.dataMetals = metals;

        let inflation1 = this.arr.filter(item => item.param_1 === 'Инфляция ИПЦ'); 
        this.dataInflation1 = inflation1;

        let inflation2 = this.arr.filter(item => item.param_1 === 'Инфляция ИЦП'); 
        this.dataInflation2 = inflation2;

        let key = this.arr.filter(item => item.param_1 === 'Ключ');
        this.dataKey = key
      },
      (error: any) => {
        console.error('Failed to add data:', error);
      }
    );
  }
  Wacc() {
    this.reference.getWacc().subscribe(
      (response: Wacc[]) => {
        this.getWacc = response
      },
    );
  }
  ParamData() {
    this.reference.getSelect().subscribe(
      (response: any[]) => {
        this.paramList = response;
  
        // const type = "[*].types[]";
        // this.getType = jmespath.search(response, type);
  
        // const name = "[*].{names:name,id:id}";
        // this.getName = jmespath.search(response, name).slice(0, -1);
      },
    );
  }
}
