import { Component, OnInit } from '@angular/core';
import { ReferencesService } from 'src/app/api-services/references.service';
import { DialogService } from 'src/app/helpers/dialog.service';
import { MenuService } from 'src/app/helpers/menu.service';
import { MessageService } from 'src/app/helpers/message.service';
import { Regions } from 'src/app/model/references/regions';

@Component({
  selector: 'app-regions',
  templateUrl: './regions.component.html',
  styleUrls: ['./regions.component.scss']
})
export class RegionsComponent implements OnInit {
  constructor(private reference: ReferencesService,public menu:MenuService,private toast:MessageService, public dialog: DialogService) { }

  ngOnInit() {
    this.getDataSourceRegion()
  }

  theader = ['№ П.П.','Наименование','Действие']
  dataRegions!: Regions[]

  getDataSourceRegion(): void {
    this.reference.getRegionReference().subscribe(
      (data: Regions[]) => {
        this.dataRegions = data
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
  }

}
