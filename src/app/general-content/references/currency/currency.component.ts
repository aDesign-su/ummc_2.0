import { Component, OnInit } from '@angular/core';
import { ReferencesService } from 'src/app/api-services/references.service';
import { DialogService } from 'src/app/helpers/dialog.service';
import { MenuService } from 'src/app/helpers/menu.service';
import { MessageService } from 'src/app/helpers/message.service';
import { Currency } from 'src/app/model/references/currency';

@Component({
  selector: 'app-currency',
  templateUrl: './currency.component.html',
  styleUrls: ['./currency.component.scss']
})
export class CurrencyComponent implements OnInit {
  constructor(private reference: ReferencesService,public menu:MenuService,private toast:MessageService, public dialog: DialogService) { }

  ngOnInit() {
    this.getDataSourceCurrency()
  }

  theader = ['№ П.П.','Наименование','Действие']
  dataCurrency!: Currency[]

  getDataSourceCurrency(): void {
    this.reference.getCurrencyReference().subscribe(
      (data: Currency[]) => {
        this.dataCurrency = data
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
  }
}
