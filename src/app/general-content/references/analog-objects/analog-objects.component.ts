import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ReferencesService } from 'src/app/api-services/references.service';
import { DialogService } from 'src/app/helpers/dialog.service';
import { MenuService } from 'src/app/helpers/menu.service';
import { MessageService } from 'src/app/helpers/message.service';
import { analogBudgets } from 'src/app/model/references/analog-objects';
import * as moment from 'moment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-analog-objects',
  templateUrl: './analog-objects.component.html',
  styleUrls: ['./analog-objects.component.scss']
})
export class AnalogObjectsComponent implements OnInit {
  constructor(private reference: ReferencesService,public menu:MenuService,private toast:MessageService, public dialog: DialogService,private formBuilder: FormBuilder,private router: Router) { 
    this.analogForm = this.formBuilder.group({
      title: "",
      smr_comp: "",
      smr_cost: null,
      date_start: null,
      date_end: null,
      pir_comp: "",
      eng_pir: null,
      oto_cost: null,
      etc_cost: null,
      tech_description: "",
      pir_cost: 0,
      project: "",
      type: "",
      region: "",
      currency: ""
    });
  }

  element!: number
  cities!: any[];
  value8: any;
  date1!: Date;
  section!: string;
  first = 0;
  rows = 10;
  
  analogForm: FormGroup;

  getCategory: any[] = [] /** Category */
  getRegion: any[] = [] /** Region */
  getCurrencyList: any[] = [] /** Currency */
  customers!: analogBudgets[];
  theader = [
    'Название',
    'Дата начала факт',
    'Дата окончания факт',
    'Стоимость', 
    'Предприятие ПИР', 
    'ПИР стоимость', 
    'Оборудование стоимость', 
    'Прочее стоимость', 
    'Предприятие СМР', 
    'СМР стоимость', 
    'Тех.характеристики', 
    'Проект реализации', 
    'Категория', 
    'Регион', 
    'Валюта', 
    'Действие'
  ]

  ngOnInit() {
    this.getDataSource()

    // Селект Category
    this.reference.getAnalogList().subscribe(
      (data: analogBudgets[]) => {
        this.getCategory = data
      },
    );
    // Селект Currency
    this.reference.getCurrencyList().subscribe(
      (data: analogBudgets[]) => {
        this.getCurrencyList = data
      },
    );
    // Селект Region
    this.reference.getRegionList().subscribe(
      (data: analogBudgets[]) => {
        this.getRegion = data
      },
    );
  }

  getDataSource(): void {
    this.reference.getAnalogBudget().subscribe((data: analogBudgets[]) => {
        this.customers = data
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
  }

    next() {this.first = this.first + this.rows;}
    prev() {this.first = this.first - this.rows;}
    reset() {this.first = 0;}
    isLastPage(): boolean {return this.customers ? this.first === (this.customers.length - this.rows): true;}
    isFirstPage(): boolean {return this.customers ? this.first === 0 : true;}

    addSection_1() {
      this.section = 'section-1'
    }
    addSection_2() {
      this.section = 'section-2'
    }
    addAnalogBudget() {
      const analogBudget = this.analogForm.value;
        const startdDate = moment(analogBudget.date_start).format('YYYY-MM-DD');
        const endDate = moment(analogBudget.date_end).format('YYYY-MM-DD');
        analogBudget.date_start = startdDate;
        analogBudget.date_end = endDate;
      this.reference.addAnalogBudget(analogBudget).subscribe(
        (response: analogBudgets[]) => {
          this.getDataSource()
          this.dialog.openDialog = false
          this.toast.access = 'Данные добавлены успешно.'
          this.toast.openMessageBar(this.toast.access, 'x', 'green-bar')
        },
        (error: any) => {
          this.toast.access = error
          this.toast.openMessageBar(this.toast.access, 'x', 'red-bar')
        }
      );
    }
    editAnalogBudget(element: any) {
      this.element = element.id;
      const selectedCurrency = this.getCurrencyList.find(currency => currency.title == element.currency);
      const selectedType = this.getCategory.find(type => type.title === element.type);
      const selectedRegion = this.getRegion.find(region => region.title === element.region);
      const startdDate = moment(element.date_start).format('DD.MM.YYYY');
      const endDate = moment(element.date_end).format('DD.MM.YYYY');

      this.analogForm.patchValue({
        type: selectedType.id,
        region: selectedRegion.id,
        currency: selectedCurrency.id,
        title: element.title,
        smr_comp: element.smr_comp,
        smr_cost: element.smr_cost,
        date_start: startdDate,
        date_end: endDate,
        pir_comp: element.pir_comp,
        eng_pir: element.eng_pir,
        oto_cost: element.oto_cost,
        etc_cost: element.etc_cost,
        tech_description: element.tech_description,
        pir_cost: element.pir_cost,
        project: element.project
      })
    }
    saveAnalogBudget() {
      if (this.element) {
        let editedObject = this.analogForm.value;
          editedObject.date_start = moment(editedObject.date_start).format('YYYY-MM-DD');
          editedObject.date_end = moment(editedObject.date_end).format('YYYY-MM-DD');
  
        this.reference.editAnalogBudget(this.element, editedObject).subscribe(
          (data: any) => {
            this.dialog.openDialog = false
            this.analogForm.reset();
            this.toast.access = 'Данные изменены успешно.'
            this.toast.openMessageBar(this.toast.access, 'x', 'green-bar')
            this.getDataSource();
          },
          (error: any) => {
            this.toast.access = error
            this.toast.openMessageBar(this.toast.access, 'x', 'red-bar')
          }
        );
      }
    }
    delAnalogBudget(element: analogBudgets) {
      const id = element.id;
      this.reference.delAnalogBudget(id).subscribe(
        (response: analogBudgets[]) => {
          this.getDataSource();
          this.toast.access = 'Данные удалены успешно.'
          this.toast.openMessageBar(this.toast.access, 'x', 'blue-bar')
        },
      );
    }
    showDetails(element: analogBudgets) {
      const setId = element.id
      if (element) {
        this.router.navigate(['analog-objects/', setId]);
      }
    }
}
