import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ReferencesService } from 'src/app/api-services/references.service';
import { DialogService } from 'src/app/helpers/dialog.service';
import { MenuService } from 'src/app/helpers/menu.service';
import { MessageService } from 'src/app/helpers/message.service';
import { Equipments, WorkPackages, analogBudgets } from 'src/app/model/references/analog-objects';

@Component({
  selector: 'app-detailed',
  templateUrl: './detailed.component.html',
  styleUrls: ['./detailed.component.scss']
})
export class DetailedComponent implements OnInit {
  constructor(private activeteRoute:ActivatedRoute,private reference: ReferencesService,public menu:MenuService,private toast:MessageService,public dialog: DialogService,private formBuilder: FormBuilder) { 
    this.getId = this.activeteRoute.snapshot.params['id']
    this.equipmenForm = this.formBuilder.group({
      title: '',
      manufacturer: '',
      tech_description: '',
      total_cost: '',
      description: '',
      analog_project: '',
      type: '',
      project: [this.getId],
      currency: ''
    });
    this.workPackage = this.formBuilder.group({
      title: [''],
      contractor: [''],
      total_cost: [null],
      mh: [null],
      comments: [''],
      volume: [null],
      unit_of_measure: [''],
      pp_cost: [0],
      type: [null],
      project: [this.getId],
      region: [null],
      currency: [null]
    });
  }

  ngOnInit() {
    this.getDataId()
    // Селект Category
    this.reference.getAnalogList().subscribe(
      (data: analogBudgets[]) => {
        this.getCategory = data
      },
    );
    // Селект Region
    this.reference.getRegionList().subscribe(
      (data: analogBudgets[]) => {
        this.getRegion = data
      },
    );
    // Селект Currency
    this.reference.getCurrencyList().subscribe(
      (data: analogBudgets[]) => {
        this.getCurrencyList = data
      },
    );
    // Селект Unit Of Measure
    this.reference.getUnitOfMeasureList().subscribe(
      (data: analogBudgets[]) => {
        this.getMeasure = data
      },
    );    
  }

  getId: number
  equipmenForm: FormGroup
  workPackage!: FormGroup;

  getMeasure: any[] = [] /** Measure */
  getRegion: any[] = [] /** Region */
  getCategory: any[] = [] /** Category */
  getCurrencyList: any[] = [] /** Currency */
  pack!: WorkPackages[];
  Equip!: Equipments[];
  theaderP = ['№ П.П.','Название','Подрядчик','Стоимость итого','Трудозатраты','Описание','Физ.объем','Ед.измерения','Стоимость удельная','Категория','Регион','Валюта','Действие']
  theaderE = ['№ П.П.','Название','Производитель','Тех.характеристики','Стоимость','Описание','Категория','Валюта','Действие']
  section!: string;

  addSection_1() {
    this.section = 'section-1'
  }
  addSection_2() {
    this.section = 'section-2'
  }

  getDataId() {
    this.reference.getAnalogBudgetId(this.getId).subscribe((data: analogBudgets) => {
      this.pack = data.children.analog_budget_package;
      this.Equip = data.children.analog_budget_equipment;
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
  }
  addEquipments() {
    const Equipment = this.equipmenForm.value;
    this.reference.addEquipment(Equipment).subscribe(
      (response: Equipments[]) => {
        this.dialog.openDialog = false
        this.getDataId()
        this.toast.access = 'Данные добавлены успешно.'
        this.toast.openMessageBar(this.toast.access, 'x', 'green-bar')
      },
      (error: any) => {
        this.toast.access = error
        this.toast.openMessageBar(this.toast.access, 'x', 'red-bar')
      }
    );
  }
  addWorkPackage() {
    const WorkPackage = this.workPackage.value;
    this.reference.addWorkPackage(WorkPackage).subscribe(
      (response: WorkPackages[]) => {
        this.dialog.openDialog = false
        this.getDataId()
        this.toast.access = 'Данные добавлены успешно.'
        this.toast.openMessageBar(this.toast.access, 'x', 'green-bar')
      },
      (error: any) => {
        this.toast.access = error
        this.toast.openMessageBar(this.toast.access, 'x', 'red-bar')
      }
    );
  }
  delEquipment(elem: any) {
    const id = elem.id;
    this.reference.delEquipment1(id).subscribe(
      (response: any[]) => {
        this.getDataId()
        this.toast.access = 'Данные удалены успешно.'
        this.toast.openMessageBar(this.toast.access, 'x', 'blue-bar')
      },
    );
  }
  delWorkPackage(elem: any) {
    const id = elem.id;
    this.reference.delWorkPackage1(id).subscribe(
      (response: any[]) => {
        this.getDataId()
        this.toast.access = 'Данные удалены успешно.'
        this.toast.openMessageBar(this.toast.access, 'x', 'blue-bar')
      },
    );
  }
}
