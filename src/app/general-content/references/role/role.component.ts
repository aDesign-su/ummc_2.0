import { Component, OnInit } from '@angular/core';
import { ReferencesService } from 'src/app/api-services/references.service';
import { DialogService } from 'src/app/helpers/dialog.service';
import { MenuService } from 'src/app/helpers/menu.service';
import { MessageService } from 'src/app/helpers/message.service';
import { Role } from 'src/app/model/references/role';

@Component({
  selector: 'app-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.scss']
})
export class RoleComponent implements OnInit {
  constructor(private reference: ReferencesService,public menu:MenuService,private toast:MessageService, public dialog: DialogService) { }

  ngOnInit() {
    this.getRoleData();
  }

  theader = ['№ П.П.','Название роли','Описание','Функционал','Уровень доступа','Доступ к данным','Действия']
  RoleArray!: Role[]

  getRoleData(): void {
    this.reference.getRoleReference().subscribe(
      (data: Role[]) => {
        this.RoleArray = data
        // console.log(this.RoleArray)
        // if (this.paginator && this.sort) {
        //   this.RoleArray.paginator = this.paginator;
        //   this.RoleArray.sort = this.sort;
        // }
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
  }

}
