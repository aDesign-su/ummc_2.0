import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ReferencesService } from 'src/app/api-services/references.service';
import { DialogService } from 'src/app/helpers/dialog.service';
import { MenuService } from 'src/app/helpers/menu.service';
import { MessageService } from 'src/app/helpers/message.service';
import { DifficultyFactor } from 'src/app/model/references/difficulty-factor';

@Component({
  selector: 'app-difficulty-factor',
  templateUrl: './difficulty-factor.component.html',
  styleUrls: ['./difficulty-factor.component.scss']
})
export class DifficultyFactorComponent implements OnInit {
  constructor(private reference: ReferencesService,private formBuilder: FormBuilder,public menu:MenuService,private toast:MessageService, public dialog: DialogService) { 
    this.ratioForm = this.formBuilder.group({
      number: "",
      rate: null,
      title: "",
      comments: ""
      });
  }

  ngOnInit() {
    this.getDataRatio()
  }

  ratioForm!: FormGroup;
  element: any;
  section!: string;
  difficultyFactor!: DifficultyFactor[]
  theader = ['№ П.П.','Обозначение','Значение','Название','Описание', 'Действие']

  addSection_1() {
    this.section = 'section-1'
  }
  addSection_2() {
    this.section = 'section-2'
  }

  getDataRatio(): void {
    this.reference.getDifficultyFactor().subscribe(
      (data: DifficultyFactor[]) => {
        this.difficultyFactor = data
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
  }
  addRatio() {
    const ratio = this.ratioForm.value;
    this.reference.addDifficultyFactor(ratio).subscribe(
      (response: DifficultyFactor[]) => {
        this.getDataRatio();
        this.dialog.openDialog = false
        this.toast.access = 'Данные добавлены успешно.'
        this.toast.openMessageBar(this.toast.access, 'x', 'green-bar')
      },
      (error: any) => {
        this.toast.access = error
        this.toast.openMessageBar(this.toast.access, 'x', 'red-bar')
      }
    );
  }
  editRatio(element: DifficultyFactor) {
    this.element = element.id; // Установим значение свойства element
    this.ratioForm.patchValue(element); // Загрузим данные в форму
    element.editing = true;
    console.log(element.id)
  }
  saveRatio() {
    if (this.element) {
      let editedObject = this.ratioForm.value;
      this.reference.editDifficultyFactor(this.element, editedObject).subscribe(
        (data: any) => {
          this.dialog.openDialog = false
          this.ratioForm.reset();
          this.toast.access = 'Данные изменены успешно.'
          this.toast.openMessageBar(this.toast.access, 'x', 'green-bar')
          this.getDataRatio();
        },
        (error: any) => {
          this.toast.access = error
          this.toast.openMessageBar(this.toast.access, 'x', 'red-bar')
        }
      );
    }
  }
  delRatio(element: DifficultyFactor) {
    const delElement = element.id
    this.reference.delDifficultyFactor(delElement).subscribe(
      (data: any) => {
        this.getDataRatio();
        this.toast.access = 'Данные удалены успешно.'
        this.toast.openMessageBar(this.toast.access, 'x', 'blue-bar')
      },
      (error: any) => {
        this.toast.access = error
        this.toast.openMessageBar(this.toast.access, 'x', 'red-bar')
      }
    )
  }
}
