import { Component, OnInit } from '@angular/core';
import { TreeNode } from 'primeng/api';
import { NodeService } from './node.service';
import { MenuService } from 'src/app/helpers/menu.service';
import { MessageService } from 'src/app/helpers/message.service';

interface Column {
  field: string;
  header: string;
}


@Component({
  selector: 'app-primavera',
  templateUrl: './primavera.component.html',
  styleUrls: ['./primavera.component.scss'],
})
export class PrimaveraComponent implements OnInit {
  constructor(private nodeService: NodeService,public menu:MenuService,private toast:MessageService) { }
    ngOnInit() {
      this.nodeService.getTreeTableNodes().then((files) => (this.files = files));

      this.cols = [
          { field: 'name', header: 'Наименование' },
          { field: 'size', header: 'Статус' },
          { field: 'type', header: 'Руководство' }
      ];
    }
    files!: TreeNode[];
    selectionKeys = {};
    cols!: Column[];

    toWbs() {
      this.toast.access = 'Данные добавлены успешно.'
      this.toast.openMessageBar(this.toast.access, 'x', 'green-bar')
    }
    fromWbs() {
      this.toast.access = 'Error.'
      this.toast.openMessageBar(this.toast.access, 'x', 'blue-bar')
    }
}
