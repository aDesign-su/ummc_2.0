import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './general-content/home/home.component';
import { PrimaveraComponent } from './general-content/primavera/primavera.component';
import { AnalogObjectsComponent } from './general-content/references/analog-objects/analog-objects.component';
import { DifficultyFactorComponent } from './general-content/references/difficulty-factor/difficulty-factor.component';
import { MacroIndicatorsComponent } from './general-content/references/macro-indicators/macro-indicators.component';
import { DetailedComponent } from './general-content/references/analog-objects/detailed/detailed.component';
import { RoleComponent } from './general-content/references/role/role.component';
import { CurrencyComponent } from './general-content/references/currency/currency.component';
import { RegionsComponent } from './general-content/references/regions/regions.component';
import { CategoryComponent } from './general-content/references/category/category.component';
import { CommonModule } from '@angular/common';
import { ProjectRegistryComponent } from './general-content/project-portfolio/project-registry/project-registry.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'primavera', component: PrimaveraComponent },
  /** Портфель проектов */
  { path: 'project-register', component: ProjectRegistryComponent },
  /** Справочники */
  { path: 'analog-objects', component: AnalogObjectsComponent },
  { path: 'difficulty-factor', component: DifficultyFactorComponent },
  { path: 'macro-indicators', component: MacroIndicatorsComponent },
  { path: 'analog-objects/:id', component: DetailedComponent },
  { path: 'role', component: RoleComponent },
  { path: 'regions', component: RegionsComponent },
  { path: 'currency', component: CurrencyComponent },
  { path: 'category', component: CategoryComponent },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    CommonModule
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
