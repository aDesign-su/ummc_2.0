export const environment = {
  production: true,
  apiUrlAuth: 'https://asbudget.ugmk.com/api/api/v1',
  apiUrl: 'https://asbudget.ugmk.com/api'
};
